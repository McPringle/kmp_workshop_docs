//
//  ContentView.swift
//  HelloKMP
//
//  Created by Mohamed Ben Hajla on 26.08.20.
//  Copyright © 2020 Mohamed Ben Hajla. All rights reserved.
//

import SwiftUI
import shared

struct ContentView: View {
    var body: some View {
        Text("\(CommonGreeter().greet())")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
