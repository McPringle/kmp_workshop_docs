package io.sunnyside.hellokmp
import  kotlin.browser.document
import org.w3c.dom.HTMLElement

fun main() {
    console.log("start")
    val message = CommonGreeter().greet()
    console.log(message)
    val root = document.getElementById("root")!! as HTMLElement
    root.innerHTML = "<h2>${message}</h2>"

}