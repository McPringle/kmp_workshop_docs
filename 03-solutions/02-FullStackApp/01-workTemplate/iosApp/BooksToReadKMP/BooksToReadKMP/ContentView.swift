//
//  ContentView.swift
//  BooksToReadKMP
//
//  Created by Mohamed Ben Hajla on 20.07.20.
//  Copyright © 2020 Mohamed Ben Hajla. All rights reserved.
//

import SwiftUI
import shared

struct ContentView: View {
    @ObservedObject var bookViewModel = BookViewModel(repository: BookRepository())

        var body: some View {
            NavigationView {
                List(bookViewModel.books, id: \.title) { book in
                    BookView(book: book)
                }
                .navigationBarTitle(Text("Books"), displayMode: .large)
                .onAppear(perform: {
                    self.bookViewModel.fetch()
                })
            }
        }
}

struct BookView : View {
    var book: Book

    var body: some View {
        HStack {
            VStack() {
                    Text(book.title).font(.headline)
                   
                }
            }
        }
    
}

/*
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
*/
 
