//
//  BookViewModel.swift
//  BooksToReadKMP
//
//  Created by Mohamed Ben Hajla on 22.07.20.
//  Copyright © 2020 Mohamed Ben Hajla. All rights reserved.
//

import Foundation
import shared

class BookViewModel: ObservableObject {
    @Published var books = [Book]()

    private let repository: BookRepository
    init(repository: BookRepository) {
        self.repository = repository
    }
    
    func fetch() {
        repository.fetchBooksFromRepo(success: { data in
            self.books = data
        })
    }
}
