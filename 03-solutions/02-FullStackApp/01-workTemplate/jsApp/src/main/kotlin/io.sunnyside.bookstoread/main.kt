package io.sunnyside.bookstoread


import io.sunnyside.common.bookstoreadkmp.BookRepository
import react.child
import react.createContext
import react.dom.render
import kotlin.browser.document



fun main() {
    document.addEventListener("DOMContentLoaded", {
        render(document.getElementById("root")) {
            app {
                
            }
        }
    })
}