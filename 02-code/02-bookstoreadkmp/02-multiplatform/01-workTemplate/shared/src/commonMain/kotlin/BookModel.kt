package io.sunnyside.common.bookstoreadkmp

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Book(
        val id:String,
        val title: String,
        val authors:List<String>,
        val notes:String,
        val bookCategory: String,
        val bookCategoryImageName: String,
        val description: String,
        val thumbnail: String
        //val webLink: String
)
