// server.js
// M. Ben Hajla sunnyside.io 2020

var http = require('http');

var books = `[
	{
	  "id": "1",
	  "title": "Show Your Work",
	  "authors": ["Austin Kleon"],
	  "notes": "",
	  "bookCategory": "Work",
	  "bookCategoryImageName": "Category_1",
	  "description": "",
	  "thumbnail": ""
	},
  
	{
	  "id": "2",
	  "title": "Building a Story Brand",
	  "authors": ["Donald Miller"],
	  "notes": "",
	  "bookCategory": "Market",
	  "bookCategoryImageName": "Category_2",
	  "description": "",
	  "thumbnail": ""
	},
  
	{
	  "id": "3",
	  "title": "The Productivity Project",
	  "authors": ["Chris Bailey"],
	  "notes": "",
	  "bookCategory": "Productive",
	  "bookCategoryImageName": "Category_3",
	  "description": "",
	  "thumbnail": ""
	},
  
	{
	  "id": "4",
	  "title": "Profit First",
	  "authors": ["Mike Michalowicz"],
	  "notes": "",
	  "bookCategory": "Money",
	  "bookCategoryImageName": "Category_4",
	  "description": "",
	  "thumbnail": ""
	},
  
	{
	  "id": "5",
	  "title": "think and grow rich",
	  "authors": ["napoleon hill"],
	  "notes": "",
	  "bookCategory": "Grow",
	  "bookCategoryImageName": "Category_5",
	  "description": "",
	  "thumbnail": ""
	},
  
	{
	  "id": "6",
	  "title": "The 7 Habits of Highly Effective People",
	  "authors": ["Stephen Covey"],
	  "notes": "",
	  "bookCategory": "Habits",
	  "bookCategoryImageName": "Category_6",
	  "description": "",
	  "thumbnail": ""
	},
  
	{
	  "id": "7",
	  "title": "The ONE Thing",
	  "authors": ["Gary Keller"],
	  "notes": "",
	  "bookCategory": "Focus",
	  "bookCategoryImageName": "Category_7",
	  "description": "",
	  "thumbnail": ""
	}
  ]  
`;
var server = http.createServer(function(req,res){


	// Set CORS headers
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', '*');
  
	res.writeHead(200, { "Content-type": "application/json" });
	res.end(books);
	return;
   
	
});


server.listen(3010, function() {
    console.log('Server is running at 3010')
});